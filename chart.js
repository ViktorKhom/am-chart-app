am4core.useTheme(am4themes_animated);

angular.module("chart", [])
  .controller('pieChart',['$scope', function ($scope) {
    $scope.chartId = '124124';
    $scope.chartType = 'PieChart';
    $scope.chartValueX = 'country';
    $scope.chartValueY = 'litres';
    $scope.chartData = [{
                          "country": "Lithuania",
                          "litres": 501.9
                        }, {
                          "country": "Czech Republic",
                          "litres": 301.9
                        }, {
                          "country": "Ireland",
                          "litres": 201.1
                        }, {
                          "country": "Germany",
                          "litres": 165.8
                        }, {
                          "country": "Australia",
                          "litres": 139.9
                        }, {
                          "country": "Austria",
                          "litres": 128.3
                        }, {
                          "country": "UK",
                          "litres": 99
                        }, {
                          "country": "Belgium",
                          "litres": 60
                        }, {
                          "country": "The Netherlands",
                          "litres": 50
                        }];
  }])
  .controller('mixedXYChart',['$scope', function ($scope) {
    $scope.chartId = '666';
    $scope.chartType = 'XYChart';
    $scope.chartTypeSeries = 'Mixed';
    $scope.chartValueX = 'country';
    $scope.chartValueY = 'units';
    $scope.chartValueY1 = 'litres';
    $scope.chartData = [{
                          "country": "Lithuania",
                          "litres": 501.9,
                          "units": 250
                      }, {
                          "country": "Czech Republic",
                          "litres": 301.9,
                          "units": 222
                      }, {
                          "country": "Ireland",
                          "litres": 201.1,
                          "units": 170
                      }, {
                          "country": "Germany",
                          "litres": 165.8,
                          "units": 122
                      }, {
                          "country": "Australia",
                          "litres": 139.9,
                          "units": 99
                      }, {
                          "country": "Austria",
                          "litres": 128.3,
                          "units": 85
                      }, {
                          "country": "UK",
                          "litres": 99,
                          "units": 93
                      }, {
                          "country": "Belgium",
                          "litres": 60,
                          "units": 50
                      }, {
                          "country": "The Netherlands",
                          "litres": 50,
                          "units": 42
                      }];
    $scope.lineColor = '#CDA2AB';
    $scope.columnColor = '#104547';
    $scope.lineStrokeWidth = 8;
  }])
  .controller('radarChart',['$scope', function ($scope) {
    $scope.chartId = '457';
    $scope.chartType = 'RadarChart';
    $scope.chartValueX = 'country';
    $scope.chartValueY = 'litres';
    $scope.chartData = [{
                          "country": "Lithuania",
                          "litres": 501.9
                        }, {
                          "country": "Czech Republic",
                          "litres": 301.9
                        }, {
                          "country": "Ireland",
                          "litres": 201.1
                        }, {
                          "country": "Germany",
                          "litres": 165.8
                        }, {
                          "country": "Australia",
                          "litres": 139.9
                        }, {
                          "country": "Austria",
                          "litres": 128.3
                        }, {
                          "country": "UK",
                          "litres": 99
                        }, {
                          "country": "Belgium",
                          "litres": 60
                        }, {
                          "country": "The Netherlands",
                          "litres": 50
                        }];
  }])
  .directive("chart", function () {
    return {
      template: "<div class='chartdiv' style='width: 100%; height: 500px'></div>",
      link: function ($scope, element) {
        // chart properties
          var chartId = $scope.chartId || 'chartdivId';
          var childData = $scope.chartId || [];
          var chartType = $scope.chartType || 'PieChart';
          var chartTypeSeries = $scope.chartTypeSeries || '';
          var chartValueX = $scope.chartValueX || 'ValueX';
          var chartValueY = $scope.chartValueY || 'ValueY';
          var chartValueY1 = $scope.chartValueY1 || 'ValueY1';
          var chartData = $scope.chartData || [];
          var sqlservid = $scope.sqlservid || '';
          var sqlservparams = $scope.sqlservparams || {};
          var sqlservmapnames = $scope.sqlservmapnames || [];
          var sqlservresultpath = $scope.sqlservresultpath || [];
          var lineColor = $scope.lineColor || '#CDA2AB';
          var columnColor = $scope.columnColor || '#104547 ';
          var lineStrokeWidth = $scope.lineStrokeWidth || 3;
          
        // create chart
        var chartdiv = angular.element(document.querySelector(".chartdiv"));
        var newDiv = angular.element("<div>");
        newDiv.attr('id', $scope.chartId);
        newDiv.css("width", "100%");
        newDiv.css("height", "500px");
        chartdiv.append(newDiv);

        switch (chartType) {
          case "PieChart": createPieChart(); break;
          case "XYChart": createXYChart(); break;
          case "RadarChart": createRadarChart(); break;
        }

        function createXYChart(chart) {
          var chart = am4core.create($scope.chartId, am4charts.XYChart);
          chart.data = chartData;
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = chartValueX;
          categoryAxis.title.text = chartValueX;
          
          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.title.text = chartValueY;
          switch (chartTypeSeries) {
            case "Line": createXYChartLineSeries(chart); break;
            case "Column": createXYChartColumnSeries(chart); break;
            case "Mixed": createXYChartMixedSeries(chart); break;
          }
        }

        function createXYChartLineSeries() {
          var series = chart.series.push(new am4charts['LineSeries']());
          series.stroke = am4core.color("#CDA2AB");
          series.strokeWidth = 3;
          series.dataFields.valueY = chartValueY;
          series.dataFields.categoryX = chartValueX;
          series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          chart.cursor = new am4charts.XYCursor();
          chart.legend = new am4charts.Legend();
          chart.legend.position = 'top';
          series.legendSettings.labelText = chartValueX;
        }

        function createXYChartColumnSeries(chart) {
          var series = chart.series.push(new am4charts['ColumnSeries']());
          series.dataFields.valueY = chartValueY;
          series.dataFields.categoryX = chartValueX;
          series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          series.columns.template.fill = am4core.color("#104547");
          chart.cursor = new am4charts.XYCursor();
          chart.legend = new am4charts.Legend();
          chart.legend.position = 'top';
          series.legendSettings.labelText = chartValueX;
        }

        function createXYChartMixedSeries(chart) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueY = chartValueY1;
          series.dataFields.categoryX = chartValueX;
          series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          series.columns.template.fill = am4core.color(columnColor);

          var series2 = chart.series.push(new am4charts.LineSeries());
          series2.dataFields.valueY = chartValueY;
          series2.dataFields.categoryX = chartValueX;
          series2.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          series2.stroke = am4core.color(lineColor);
          series2.strokeWidth = lineStrokeWidth;
          chart.cursor = new am4charts.XYCursor();
        }

        function createPieChart() {
          var chart = am4core.create($scope.chartId, am4charts.PieChart3D);
          chart.data = chartData;
          var pieSeries = chart.series.push(new am4charts.PieSeries3D());
          pieSeries.dataFields.value = chartValueY;
          pieSeries.dataFields.category = chartValueX;
        }

        function createRadarChart() {
          var chart = am4core.create($scope.chartId, am4charts.RadarChart);
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = chartValueX;
          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          var series = chart.series.push(new am4charts.RadarSeries());
          chart.data = chartData;
          series.dataFields.valueY = chartValueY;
          series.dataFields.categoryX = chartValueX;
        }

        $scope.$on("$destroy", function () {
          chart.dispose();
        });
      }
    };
  });